import { expect } from "chai";
import Ajv from "ajv";
import chai from "chai";

chai.use(require("chai-json-schema"));
const ajv = new Ajv();

export function checkResponseAgainstSchema(data: any, schema: any) {
  if (Array.isArray(data)) {
    expect(data).to.be.jsonSchema(schema);

    data.forEach((item, index) => {
      expect(item).to.be.jsonSchema(schema.items);
    });
  } else if (typeof data === "object") {
    expect(data).to.be.jsonSchema(schema);
  } else {
    throw new Error(
      "Invalid response data type. Expected an array or an object."
    );
  }
}

function checkObjectResponseAgainstSchema(data: any, schema: any) {
  expect(data).to.be.jsonSchema(schema);
}

export function checkStatusCode(
  response,
  statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500
) {
  expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(
    statusCode
  );
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
  expect(
    response.timings.phases.total,
    `Response time should be less than ${maxResponseTime}ms`
  ).to.be.lessThan(maxResponseTime);
}
