import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkResponseAgainstSchema,
} from "../../helpers/functionsForChecking.helper";
import {
  schema_allUsers,
  schema_allPosts,
  schema_post,
} from "../specs/data/schemas_testData.json";
import Chance from "chance";
import chai from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import Ajv from "ajv";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const posts = new PostsController();
const chance = new Chance();
chai.use(require("chai-json-schema"));
const ajv = new Ajv();

let registeredEmail: string;
let registeredPassword: string;
let registeredUserAvatar: string;
let registeredUsername: string;
let registeredUserId: number;
let accessToken: string;
let loginResponse: any;
let postId: number;
let body: string;

describe("End2end user flow", () => {
  before("Register and login", async () => {
    registeredEmail = chance.email();
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredEmail,
      registeredPassword,
      registeredUsername
    );
    registeredUserId = registrationResponse.body.user.id;
    registeredUserAvatar = registrationResponse.body.user.avatar;

    loginResponse = await auth.login(registeredEmail, registeredPassword);
    accessToken = loginResponse.body.token.accessToken.token;
  });

  it("Get all users", async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.length).to.be.greaterThan(0);
    checkResponseAgainstSchema(response.body, schema_allUsers);
  });

  it("Get current user by id", async () => {
    const expectedUserData = {
      id: registeredUserId,
      avatar: "string",
      email: registeredEmail,
      userName: registeredUsername,
    };
    const response = await users.getUserById(registeredUserId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body).to.deep.equal(expectedUserData);
  });

  it("Get all posts", async () => {
    const response = await posts.getAllPosts();
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body).to.be.an("array").that.is.not.empty;
    checkResponseAgainstSchema(response.body, schema_allPosts);
  });

  it("Create post", async () => {
    body = chance.string({
      length: 20,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    const newPost = {
      authorId: registeredUserId,
      previewImage: undefined,
      body: body,
    };
    const response = await posts.createPost(newPost, accessToken);
    postId = response.body.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.body).to.equal(newPost.body);
    checkResponseAgainstSchema(response.body, schema_post);
  });

  it("Like post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.likePost(newPostLike, accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getAllPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(addedLike).to.exist;
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
