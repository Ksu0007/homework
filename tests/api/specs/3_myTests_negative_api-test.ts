import { expect } from "chai";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import { checkResponseTime } from "../../helpers/functionsForChecking.helper";
import Chance from "chance";

const auth = new AuthController();
const reg = new RegController();
const chance = new Chance();
const INVALID_PASSWORD_ERROR_MESSAGE = "Invalid username or password.";

let registeredEmail: string;
let registeredPassword: string;
let registeredUsername: string;

describe("Negative Path Tests user", () => {
  let loginResponse: any;

  it(`Registration with invalid email`, async () => {
    const invalidEmail = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      invalidEmail,
      registeredPassword,
      registeredUsername
    );

    expect(registrationResponse.statusCode).to.equal(400);
    expect(registrationResponse.body.errors).to.exist;
    expect(registrationResponse.body.errors[0]).to.be.a("string");
    checkResponseTime(registrationResponse, 1000);
  });

  it("Registration with invalid password - 3 characters", async () => {
    registeredEmail = chance.email();
    const invalidShortPassword = chance.string({
      length: 3,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredEmail,
      invalidShortPassword,
      registeredUsername
    );

    expect(registrationResponse.statusCode).to.equal(400);
    expect(registrationResponse.body.errors).to.exist;
    expect(registrationResponse.body.errors[0]).to.be.a("string");
    checkResponseTime(registrationResponse, 1000);
  });

  it("Authorisation with invalid password", async () => {
    registeredEmail = chance.email();
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    await reg.registration(
      registeredEmail,
      registeredPassword,
      registeredUsername
    );

    let invalidCredentialsDataSet = [
      { email: registeredEmail, password: "" },
      { email: registeredEmail, password: "      " },
      { email: registeredEmail, password: "ATest2023! " },
      { email: registeredEmail, password: "ATest 2021" },
      { email: registeredEmail, password: "admin" },
      { email: registeredEmail, password: registeredEmail },
    ];
    invalidCredentialsDataSet.forEach(async (credentials) => {
      loginResponse = await auth.login(credentials.email, credentials.password);

      expect(loginResponse.statusCode).to.equal(401);
      expect(loginResponse.body.error).to.exist;
      expect(loginResponse.body.error).to.equal(INVALID_PASSWORD_ERROR_MESSAGE);
    });
  });
});
