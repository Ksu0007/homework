import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkResponseAgainstSchema,
} from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import "chai-json-schema";
import {
  schema_allUsers,
  schema_login,
  schema_user,
} from "../specs/data/schemas_testData.json";
import chai from "chai";
import Ajv from "ajv";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const chance = new Chance();
chai.use(require("chai-json-schema"));
const ajv = new Ajv();

let registeredEmail: string;
let registeredPassword: string;
let registeredUserAvatar: string;
let registeredUsername: string;
let registeredUserId: number;
let accessToken: string;
let loginResponse: any;
let updatedUserData: any;

describe("Happy Path Tests user", () => {
  before("Register and login", async () => {
    registeredEmail = chance.email();
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredEmail,
      registeredPassword,
      registeredUsername
    );
    registeredUserId = registrationResponse.body.user.id;
    registeredUserAvatar = registrationResponse.body.user.avatar;

    loginResponse = await auth.login(registeredEmail, registeredPassword);
    accessToken = loginResponse.body.token.accessToken.token;
  });

  it(`User is registered and logged in`, async () => {
    expect(loginResponse).to.exist;
    let userData = loginResponse.body.user;

    expect(userData.id).to.equal(registeredUserId);
    expect(userData.email).to.equal(registeredEmail);
    expect(userData.userName).to.equal(registeredUsername);

    checkStatusCode(loginResponse, 200);
    checkResponseTime(loginResponse, 1000);
    checkResponseAgainstSchema(loginResponse.body, schema_login);
  });

  it("Get all users", async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.length).to.be.greaterThan(0);
    checkResponseAgainstSchema(response.body, schema_allUsers);
  });

  it("Get current user details", async () => {
    const response = await users.getCurrentUser(accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkResponseAgainstSchema(response.body, schema_user);
  });

  it("Update current user", async () => {
    function replaceLastThreeWithRandom(str: string): string {
      return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
    }

    const updatedUserData = {
      id: registeredUserId,
      avatar: registeredUserAvatar,
      email: registeredEmail,
      userName: replaceLastThreeWithRandom(registeredUsername),
    };
    const response = await users.updateUser(updatedUserData, accessToken);

    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
    updatedUserData.userName = updatedUserData.userName;
  });

  it("Get current user details again", async () => {
    const response = await users.getCurrentUser(accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.user).to.be.deep.equal(
      updatedUserData,
      "User details isn't correct"
    );
  });

  it("Get current user by id", async () => {
    const response = await users.getUserById(registeredUserId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.user).to.be.deep.equal(
      updatedUserData,
      "User details isn't correct"
    );
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
