import { expect } from "chai";
import chai from "chai";

import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkResponseAgainstSchema,
} from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import {
  schema_post,
  schema_comment,
  schema_allPosts,
} from "../specs/data/schemas_testData.json";
import { PostsController } from "../lib/controllers/posts.controller";
import Ajv from "ajv";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const posts = new PostsController();
const chance = new Chance();
chai.use(require("chai-json-schema"));
const ajv = new Ajv();

let registeredEmail: string;
let registeredPassword: string;
let registeredUsername: string;
let registeredUserId: number;
let accessToken: string;
let body: string;
let postId: number;
let commentBody: string;
let loginResponse: any;

describe("Happy Path Tests post", () => {
  before("Register and login", async () => {
    registeredEmail = chance.email();
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredEmail,
      registeredPassword,
      registeredUsername
    );
    registeredUserId = registrationResponse.body.user.id;

    loginResponse = await auth.login(registeredEmail, registeredPassword);
    accessToken = loginResponse.body.token.accessToken.token;
  });

  it("Create post", async () => {
    body = chance.string({
      length: 20,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    const newPost = {
      authorId: registeredUserId,
      previewImage: undefined,
      body: body,
    };
    const response = await posts.createPost(newPost, accessToken);
    postId = response.body.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkResponseAgainstSchema(response.body, schema_post);
    expect(response.body.body).to.equal(newPost.body);
  });

  it("Check post creation", async () => {
    const allPostsResponse = await posts.getAllPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    expect(createdPost).to.exist;
  });

  it("Like post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.likePost(newPostLike, accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getAllPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(addedLike).to.exist;
  });

  it("Dislike post", async () => {
    const newPostDislike = {
      entityId: postId,
      isLike: false,
      userId: registeredUserId,
    };

    const response = await posts.likePost(newPostDislike, accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getAllPosts();
    const allPosts = allPostsResponse.body;

    const createdPost = allPosts.find((post) => post.id === postId);

    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(addedLike).to.not.exist;
  });

  it("Add comment", async () => {
    commentBody = "Wow2!!!!";
    const newComment = {
      authorId: registeredUserId,
      postId: postId,
      body: commentBody,
    };

    const addCommentResponse = await posts.addComment(newComment, accessToken);

    checkStatusCode(addCommentResponse, 200);
    checkResponseTime(addCommentResponse, 1000);
    expect(addCommentResponse.body.body).to.be.deep.equal(commentBody);
    checkResponseAgainstSchema(addCommentResponse.body, schema_comment);
  });

  it("Get all posts", async () => {
    const response = await posts.getAllPosts();
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body).to.be.an("array").that.is.not.empty;
    checkResponseAgainstSchema(response.body, schema_allPosts);
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
